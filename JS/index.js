/**
 * CLASSES
 */

class Bank {
    /**Fields */
    #balance = 0;
    #loanAmount = 0;
    #loanTaken = false;

    /**Properties */
    getBalance() {
        return this.#balance;
    }
    getLoan() {
        return this.#loanAmount;
    }
    setLoan(loan) {
        this.#loanAmount = loan;
    }
    isLoanTaken() {
        return this.#loanTaken;
    }
    setLoanTaken(loanTaken) {
        this.#loanTaken = loanTaken;
    }

    /**Methods */
    addToBalance(amount) {
        this.#balance += amount;
    }
    removeFromBalance(amount) {
        this.#balance -= amount;
    }

    removeFromLoan(amount) {
        if (this.#loanAmount - amount <= 0) {
            amount -= this.#loanAmount;
            this.#loanAmount = 0;
            this.#loanTaken = false;
            return amount;
        }
        this.#loanAmount -= amount;
        return null;
    }
}

class Salary {
    /** Fields */
    #amount = 0;

    /** Properties */
    getAmount() {
        return this.#amount;
    }
    setAmount(value) {
        this.#amount = value;
    }

    /** Methods */
    addAmount(amount) {
        this.#amount += amount;
    }

    subtractAmount(amount) {
        this.#amount -= amount;
    }

}

/**
 * VARIABLES
 */
const bank = new Bank();
const salary = new Salary();
const laptops = await fetchLaptops();

let selectedLaptop = null;

/**
 * DOM ELEMENTS
 */
/* Texts */
const balanceAmountElement = document.getElementById("balance-amount");
const loanAmountElement = document.getElementById("loan-amount");

const salaryAmountElement = document.getElementById("pay-text");

const laptopsElement = document.getElementById("laptop-select");
const laptopImageElement = document.getElementById("laptop-img");
const laptopNameElement = document.getElementById("laptop-name");
const laptopDescriptionElement = document.getElementById("laptop-description");
const laptopPriceElement = document.getElementById("laptop-price");

/* Containers */
const loanContainerElement = document.getElementById("loan-container");
const selectedLaptopFeaturesElement = document.getElementById("selected-laptop-features-container");
const laptopContainerElement = document.getElementById("laptop-information-row");

/* Buttons */
const receiveLoanButton = document.getElementById("receive-loan-button");

const transferToBankButton = document.getElementById("transfer-to-bank-button");
const workButton = document.getElementById("work-button");
const payLoanButton = document.getElementById("pay-loan-button");

const buyLaptopButton = document.getElementById("buy-laptop-button");

/**
 * FUNCTIONS
 */
//Init sets the initial bank balance and salary on the HTML to their respecive values (0)
//It also hides any elements that should not be visible to the user when the html page is loaded
function init() {
    setBankBalance();
    setSalary();

    hideLoanElements();
    hideLaptopInformation();
    hideLaptopFeatures();
}

init();

// fetchLaptops is called immediately by the laptops variable
// This method fetches the laptops from the api provided by Noroff
// It returns the contents of the json object and which is stored in the laptops variable.
async function fetchLaptops() {
    try {
        const response = await fetch(
            "https://noroff-komputer-store-api.herokuapp.com/computers"
        );

        const json = await response.json();
        return json;
    } catch (error) {
        console.error("Error: ", error.message);

    }
}

// Runs afters the fetchLaptops method
// adds all laptops to the laptop selection dropdown menu
for (const laptop of laptops) {
    const html = `<option value=${laptop.id}>${laptop.title}</option>`;
    laptopsElement.insertAdjacentHTML("beforeend", html);
}


// Adds a value of 100 to the Salary amount
// It will then update the HTML with setSalary()
// addSalary is called by the event listener on the Work button 
function addSalary() {
    const amount = 100;
    salary.addAmount(amount);

    setSalary();
}

// Adds the value of the Salary amount to the bank.
// It will then update the relevant HTML elements
// If a loan has been taken, it will deduct 10% from the value
// This 10% is deducted from the Loan amount stored in the Bank
// If there are any leftover, the loan has been paid in full (even if it is 0) 
// If this is the case, any elements regarding the loan are hidden again.
// Deposit Salary is called by the Deposit to Bank button
function depositSalary() {
    let salaryAmount = salary.getAmount();

    if (bank.isLoanTaken()) {
        const amountToLoanPayment = salaryAmount * 0.1;
        salaryAmount -= amountToLoanPayment;
        const leftovers = bank.removeFromLoan(amountToLoanPayment);
        if (leftovers != null) {
            hideLoanElements();
        }
        setLoanAmount();
    }

    bank.addToBalance(salaryAmount);
    salary.setAmount(0);

    setBankBalance();
    setSalary();

}

// Shows the User a prompt and asks how much they which to loan
// It will alert the user if the amount asked for is more than twice their balance in the bank
// If this is not the case, it will add the loan to the loan amount (used with Salary)
// It will set the Loan Taken boolean to true and add the loan to the bank balance.
// And update relevant HTML elements
// Called by the Get Loan button
function startLoanPrompt() {
    // Loan already taken, cannot take two loans
    if (bank.isLoanTaken()) {
        alert("Loan is already taken, pay back first");
        return;
    }

    let loanAmount = prompt("How would you like to loan?", 0);

    // Checks wether the loanAmount is Not A Number
    // If this is true, it will alert the user and return
    if(isNaN(loanAmount))
    {
        alert("Loan is not a number. Please fill in a number.");
        return
    }

    loanAmount = parseInt(loanAmount); //Change amount from string to number

    // Loan amount cannot be more than twice the amount of the bank balance
    if (loanAmount > bank.getBalance() * 2) {
        alert("Loan is to high, cannot double bank balance. Ask again for a lower loan.");
        return;
    }

    bank.setLoan(loanAmount);
    bank.setLoanTaken(true);
    bank.addToBalance(loanAmount);

    showLoanElements();
    setBankBalance();
    setLoanAmount();
}


// Deduct the entire Salary from the Loan Amount
// If there are any leftovers, it means the loan has been paid in full
// Any remaining value is added to the bank balance
// Relevant HTML elements are updated
// Called by the Pay Loan button
function salaryToLoanPayment() {
    const salaryAmount = salary.getAmount();
    const leftovers = bank.removeFromLoan(salaryAmount);

    salary.setAmount(0);
    setSalary();
    setLoanAmount();

    //Leftover != null means there are leftover afters deduction which means the loan
    //has been paid in full, anything left should be added to the bank
    if (leftovers != null) {
        bank.addToBalance(leftovers);
        setBankBalance();
        hideLoanElements();
    }
}

// Function loops through the laptops array in order to find the selected laptop from the dropdown
// setLaptop handles the rest
// Called by changing the value of the dropdown menu
/**@type HTMLSelectElement */
function onSelectChangeLaptop() {
    const laptopId = this.value;
    console.log(laptopId + " " + this.value);
    laptops.forEach(laptop => {
        if (laptop.id == laptopId) {
            setLaptop(laptop)
        }
    });
}

// Checks wether the bank balance is not lower than the price of the selected laptop from the dropdown menu
// If this is not the case, the value of the price is deducted from the bank balance
// User is alerted on their new purchase
// Relevant HTML elements are updated
// Called by the Buy Laptop button
function buyLaptop() {
    if(bank.getBalance() < selectedLaptop.price)
    {
        alert("Not enough Virtual Tostis! Work harder");
        return;
    }

    bank.removeFromBalance(selectedLaptop.price);
    alert(`You are now the proud owner of: ${selectedLaptop.title}`);

    setBankBalance();
}



/**
 * RENDERERS
 */
function setBankBalance() {
    balanceAmountElement.innerHTML = `${bank.getBalance()} Virtual Tostis`;
}

function setSalary() {
    salaryAmountElement.innerHTML = `${salary.getAmount()} Virtual Tostis`;
}

function setLoanAmount() {
    loanAmountElement.innerHTML = `${bank.getLoan()} Virtual Tostis`;
}

function hideLoanElements() {
    if (bank.isLoanTaken != false) {
        loanContainerElement.style.display = "none";
        payLoanButton.style.display = "none";

    }
}

function showLoanElements() {
    loanContainerElement.style.display = "flex";
    payLoanButton.style.display = "flex";
}

// function HideLoanButton() {
//     receiveLoanButtonElement.style.display = "none";
// }

// function ShowLoanButton() {
//     receiveLoanButtonElement.style.display = "flex";
// }

function hideLaptopFeatures() {
    selectedLaptopFeaturesElement.style.display = "none";
}

// Loops the through the specs array of the selected laptop
// Shows relevant information to the user
// Stores the laptop in the selectedLaptop field for further use
// Called when an item is selected in the dropdown menu
function setLaptop(laptop) {
    selectedLaptopFeaturesElement.style.display = "block";
    laptopContainerElement.style.display = "block";

    //FEATURES
    selectedLaptopFeaturesElement.innerHTML = "";
    laptop.specs.forEach(spec => {
        const html = `<li>${spec}</li>`;
        selectedLaptopFeaturesElement.insertAdjacentHTML("beforeend", html);
    });

    //IMAGE
    const baseLink = "https://noroff-komputer-store-api.herokuapp.com/";
    laptopImageElement.setAttribute("src", baseLink + laptop.image);

    laptopNameElement.innerHTML = laptop.title;
    laptopDescriptionElement.innerHTML = laptop.description;
    laptopPriceElement.innerHTML = `${laptop.price} Virtual Tostis`;

    selectedLaptop = laptop;
}


function hideLaptopInformation() {
    laptopContainerElement.style.display = "none";
}

/**
 * ERROR HANDLERS
 */
// Invoked when the laptop image cannot be laptop
function laptopImageError() {
    laptopImageElement.setAttribute("src", "laptopnotfound.jpg");

}

/**
 * EVENT HANDLERS
 */
workButton.addEventListener("click", addSalary);
transferToBankButton.addEventListener("click", depositSalary);
receiveLoanButton.addEventListener("click", startLoanPrompt)
payLoanButton.addEventListener("click", salaryToLoanPayment);
buyLaptopButton.addEventListener("click", buyLaptop);

laptopsElement.addEventListener("change", onSelectChangeLaptop)

laptopImageElement.addEventListener("error", laptopImageError);